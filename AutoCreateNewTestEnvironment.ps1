﻿Set-ExecutionPolicy -ExecutionPolicy RemoteSigned

#Adjustable Variables used in Script

$NewTestDate = Get-date
##$ObjectFilePath = "\\sgfile02\IT\NAVRelated\AutoTestEnvironment\Objects\Test_" + $NewTestDate.ToShortDateString() + ".fob"
$ObjectPath = "\\sgfile02\IT\NAVRelated\AutoTestEnvironment\Objects\"+$NewTestDate.Year+"_"+$NewTestDate.Month+"_"+$NewTestDate.Day
$PermissionPath = "\\sgfile02\IT\NAVRelated\AutoTestEnvironment\Permissions\"
##$ObjectFilePath = "\\sgfile02\IT\NAVRelated\AutoTestEnvironment\Objects\"+$NewTestDate.Year+"_"+$NewTestDate.Month+"_"+$NewTestDate.Day+"\NAV Test Object backup.fob"
$ObjectFilePath = $objectpath +"\NAV Test Object backup.fob"
#Folder DATE needs to be created first
$TestMessageArgument = "TESTENVIRONMENT|14-11-2018|12:00:00"
$ServiceTierInstance = 'Bjarne'    #'testrit'
$DatabaseServer = "SGTESTSQL01"
$DatabaseName = 'SGITest2013HQ' #"Nav2017Test" #"SGITest2013HQ"
$NavIde = 'C:\Program Files\Microsoft Dynamics NAV\100\TestRIT\finsql.exe'


#Import Nav Admin modules
Import-Module 'C:\Program Files\Microsoft Dynamics NAV\100\TestRIT\NavAdminTool.ps1'
Import-Module 'C:\Program Files\Microsoft Dynamics NAV\100\TestRIT\microsoft.dynamics.nav.management.psd1'
Import-Module 'C:\Program Files\Microsoft Dynamics NAV\100\TestRIT\microsoft.dynamics.nav.apps.management.psd1'
#Import Nav Dev. modules
Import-Module 'C:\Program Files (x86)\Microsoft Dynamics NAV\100\RoleTailored Client\NavModelTools.ps1'
Import-Module 'C:\Program Files (x86)\Microsoft Dynamics NAV\100\RoleTailored Client\microsoft.dynamics.nav.model.tools.psd1'
#Import SQLPS Module
#Import-Module '\\sgfile02\it\NAVRelated\Dynateam\BDP\AutoTest\SQLPS\SQLPS.PSD1'
#Check exists
#if (-Not Get-Module -ListAvailable -Name SqlServer)
#{
    Install-Module -Name SqlServer
#}

#Step 2 - Notify users through email
#OTRS: 2018082110000233
#
#NAV can only receive one argument, as a result we have 3 arguments seperated by | in 1 string
#
#MessageType: TestEnvironment    (Triggers notification for new test environment)
#|
#MessageDate:  Date for the update
#|
#MessageTime: Expected time for the update.
Invoke-NAVCodeunit -ServerInstance $ServiceTierInstance -CompanyName SGI -CodeunitId 66105 -MethodName PublishUserMessage -Argument $TestMessageArgument

#Step 3 - Backup objects from Test DB
$NavIde = 'C:\Program Files\Microsoft Dynamics NAV\100\TestRIT\finsql.exe'
if(!(Test-Path -Path $objectpath )){
    New-Item -ItemType directory -Path $ObjectPath }
Export-NAVApplicationObject -DatabaseServer $DatabaseServer -DatabaseName $DatabaseName -Path $ObjectFilePath -force


#Step 4 - Export permissions
Invoke-NAVCodeunit -ServerInstance $ServiceTierInstance -CompanyName SGI -CodeunitId 66106 -MethodName ExportPermissionData -Argument $permissionpath

#Step 5 - Export TEST licens, skipped as is handled by license management
#Requires Drift to run serverbased license (NOT database license)
#Fixed as pr. October 2018

 #Step 6 - Make SQL Backup
 Invoke-Sqlcmd -ServerInstance "SGSQL02" -Database "SGIDrift2013R2" -QueryTimeout 0 -InputFile "\\sgfile02\it\NAVRelated\Dynateam\BDP\AutoTest\BackupLiveDatabase.sql" | Out-File "\\sgfile02\it\NAVRelated\Dynateam\BDP\AutoTest\BackupLiveDatabaseStatus.rpt" 

 #Step 7 - Stop Service tiers
 #Get-Service -DisplayName 'Microsoft Dynamics NAV Server *' | Stop-Service
 #Get-Service -DisplayName '*Test*' | Stop-Service
 Get-Service -DisplayName '*Bjarne*' | Stop-Service

 #Step 8 - Restore SQL Backup to test database
 Invoke-Sqlcmd -ServerInstance "SGTESTSQL01" -QueryTimeout 0 -InputFile "\\sgfile02\it\NAVRelated\Dynateam\BDP\AutoTest\RestoreLiveDatabaseInTest.sql" | Out-File "\\sgfile02\it\NAVRelated\Dynateam\BDP\AutoTest\RestoreLiveDatabaseInTest.rpt"

  #Step 15 - Start service tiers
 #
 # - New test tier during development (Bjarne)
 #
 #Get-Service -DisplayName 'Microsoft Dynamics NAV Server *' | Start-Service
 #Get-Service -DisplayName '*Test*' | Start-Service
 Get-Service -DisplayName '*Bjarne*' | Start-Service

 #Step 10 - Import Object & Permissions
 Import-NAVApplicationObject -NavServerName "SGTESTTIER01" -NavServerInstance $ServiceTierInstance -NavServerManagementPort 8445 -DatabaseServer $DatabaseServer -DatabaseName $DatabaseName -Path $ObjectFilePath -SynchronizeSchemaChanges Force -ImportAction Overwrite
 Invoke-NAVCodeunit -ServerInstance $ServiceTierInstance -CompanyName SGI -CodeunitId 66106 -MethodName ImportPermissionData -Argument $permissionpath

 #Step 11 - Deactivate settings / Paths from live
 invoke-navcodeunit -serverinstance $ServiceTierInstance -CompanyName SGI -CodeunitId 66107

 #Step 14 - Enable External users in Test
 Invoke-NAVCodeunit -ServerInstance $ServiceTierInstance -CompanyName SGI -CodeunitId 66106 -MethodName EnableTestUsers 



 #Step 16 + 17 - Handled by SGI

 #Step 18 - Update System Indicator
 Invoke-NAVCodeunit -ServerInstance $ServiceTierInstance -CompanyName SGI -CodeunitId 66106 -MethodName UpdateSystemIndicator -Argument " "
