OBJECT Codeunit 66105 UserInteractionMgt
{
  OBJECT-PROPERTIES
  {
    Date=29-08-18;
    Time=10:18:55;
    Modified=Yes;
    Version List=RIT.001,AutoEnv;
  }
  PROPERTIES
  {
    OnRun=BEGIN
            PublishUserMessage('TESTENVIRONMENT|30-08-2018|12:00:00');
          END;

  }
  CODE
  {

    [Integration]
    PROCEDURE PublishUserMessage@1000000000(MessageContent@1000000000 : Text);
    BEGIN
    END;

    BEGIN
    {
      RIT.001 29-08-2018  BDP [2018082110000233]
                              - Created Publisher Function
    }
    END.
  }
}

