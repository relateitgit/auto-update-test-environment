OBJECT Table 66122 AutoEnvironmentSetup
{
  OBJECT-PROPERTIES
  {
    Date=29-08-18;
    Time=11:08:04;
    Modified=Yes;
    Version List=RIT.001,AutoEnv;
  }
  PROPERTIES
  {
    CaptionML=[DAN=Ops�tning af Automatisk Milj�;
               ENU=Automatic Environment Setup];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10         }
    { 10  ;   ;SenderEmail         ;Text30        ;CaptionML=[DAN=Afsender Email;
                                                              ENU=Sender Email] }
    { 11  ;   ;SenderEmailName     ;Text30        ;CaptionML=[DAN=Afsender Navn;
                                                              ENU=Sender Name] }
    { 12  ;   ;EmailSubject        ;Text30        ;CaptionML=[DAN=Email Emne;
                                                              ENU=Email Subject] }
    { 50  ;   ;NewTestEnvironmentMsgType;Code20   ;Description=RIT.001 }
    { 100 ;   ;NewTestEnvironmentText;Text250     ;CaptionML=[DAN=Nyt Testmilj�;
                                                              ENU=New Test Environment];
                                                   Description=RIT.001 }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      RIT.001 29-08-2018  BDP [2018082110000233]
                              - Created
    }
    END.
  }
}

OBJECT Codeunit 66105 UserInteractionMgt
{
  OBJECT-PROPERTIES
  {
    Date=29-08-18;
    Time=10:18:55;
    Modified=Yes;
    Version List=RIT.001,AutoEnv;
  }
  PROPERTIES
  {
    OnRun=BEGIN
            PublishUserMessage('TESTENVIRONMENT|30-08-2018|12:00:00');
          END;

  }
  CODE
  {

    [Integration]
    PROCEDURE PublishUserMessage@1000000000(MessageContent@1000000000 : Text);
    BEGIN
    END;

    BEGIN
    {
      RIT.001 29-08-2018  BDP [2018082110000233]
                              - Created Publisher Function
    }
    END.
  }
}

OBJECT Codeunit 66106 AutoEnvironMgt
{
  OBJECT-PROPERTIES
  {
    Date=30-10-18;
    Time=13:54:05;
    Modified=Yes;
    Version List=RIT.001,AutoEnv;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      ImportExportPermissionSets@1000000000 : XMLport 9171;
      ImportExportPermissions@1000000001 : XMLport 9172;
      DestFile@1000000002 : File;
      oStream@1000000003 : OutStream;
      iStream@100000000 : InStream;

    PROCEDURE ExportPermissionData@1000000000(DestinationPath@1000000000 : Text);
    BEGIN
      CLEAR(ImportExportPermissionSets);
      CLEAR(ImportExportPermissions);

      CheckDestinationPath(DestinationPath);

      CLEAR(DestFile);
      CLEAR(oStream);
      DestFile.WRITEMODE(TRUE);
      DestFile.CREATE(DestinationPath + 'NAV Test Permissions.xml');
      DestFile.CREATEOUTSTREAM(oStream);

      CLEAR(ImportExportPermissions);
      ImportExportPermissions.SETDESTINATION(oStream);
      ImportExportPermissions.EXPORT();
      DestFile.CLOSE;

      CLEAR(DestFile);
      CLEAR(oStream);
      DestFile.WRITEMODE(TRUE);
      DestFile.CREATE(DestinationPath + 'NAV Test PermissionSets.xml');
      DestFile.CREATEOUTSTREAM(oStream);

      CLEAR(ImportExportPermissionSets);
      ImportExportPermissionSets.SETDESTINATION(oStream);
      ImportExportPermissionSets.EXPORT();
      DestFile.CLOSE;
    END;

    PROCEDURE ImportPermissionData@1000000001(DestinationPath@100000000 : Text);
    BEGIN
      CLEAR(ImportExportPermissionSets);
      CLEAR(ImportExportPermissions);

      CheckDestinationPath(DestinationPath);

      CLEAR(DestFile);
      CLEAR(iStream);
      DestFile.WRITEMODE(FALSE);
      DestFile.CREATEINSTREAM(iStream);
      DestFile.OPEN(DestinationPath + 'NAV Test Permissions.xml');

      CLEAR(ImportExportPermissions);
      ImportExportPermissions.SETSOURCE(iStream);
      ImportExportPermissions.IMPORT();
      DestFile.CLOSE;

      CLEAR(DestFile);
      CLEAR(oStream);
      DestFile.WRITEMODE(FALSE);
      DestFile.CREATEINSTREAM(iStream);
      DestFile.OPEN(DestinationPath + 'NAV Test PermissionSets.xml');

      CLEAR(ImportExportPermissionSets);
      ImportExportPermissionSets.SETSOURCE(iStream);
      ImportExportPermissionSets.IMPORT();
      DestFile.CLOSE;
    END;

    LOCAL PROCEDURE CheckDestinationPath@1000000002(VAR DestinationPath@1000000000 : Text);
    BEGIN
      IF COPYSTR(DestinationPath,STRLEN(DestinationPath) - 1) <> '\' THEN
        DestinationPath := DestinationPath + '\';

    END;

    PROCEDURE EnableTestUsers@1000000003() : Text;
    VAR
      AccessControl@1000000001 : Record 2000000053;
      User@1000000000 : Record 2000000120;
      Counter@1000000002 : Integer;
      txtResult@1000000003 : TextConst 'DAN=%1 brugere aktiveret ud fra %2;ENU=%1 users enabled based on %2';
    BEGIN
      Counter := 0;

      CLEAR(AccessControl);
      AccessControl.SETRANGE("Role ID",'SGI TEST ACCESS');
      IF AccessControl.FINDSET THEN REPEAT
        IF NOT User.GET(AccessControl."User Security ID") THEN
          CLEAR(User);

        User.VALIDATE(State,User.State::Enabled);
        User.VALIDATE("Expiry Date",CREATEDATETIME(0D,0T));
        User.MODIFY(TRUE);
        Counter += 1;
      UNTIL AccessControl.NEXT = 0;


      MESSAGE(txtResult,Counter,'SGI TEST ACCESS'); //Message for Powershell console
    END;

    PROCEDURE UpdateSystemIndicator@1000000004();
    VAR
      CompanyInformation@1000000000 : Record 79;
      Company@1000000001 : Record 2000000006;
    BEGIN
      CLEAR(Company);
      IF Company.FINDSET THEN REPEAT
        CompanyInformation.CHANGECOMPANY(Company.Name);
        CompanyInformation.GET;
        CompanyInformation."System Indicator" := CompanyInformation."System Indicator"::"Custom Text";
        CompanyInformation."Custom System Indicator Text" := 'TEST HQ Environment created ' + FORMAT(TODAY);
        CompanyInformation.MODIFY;
      UNTIL Company.NEXT = 0;
    END;

    BEGIN
    END.
  }
}

OBJECT Codeunit 66107 SetPathSettingsTest
{
  OBJECT-PROPERTIES
  {
    Date=30-10-18;
    Time=09:28:29;
    Modified=Yes;
    Version List=RIT.001,AutoEnv;
  }
  PROPERTIES
  {
    OnRun=BEGIN
            JobQueue();
            LSTaskQueue();
            DocumentCapture();
            ExpenseManagement();
            FTP();
            WarehouseScanner();
            WarehouseTablet();
            Lector();
            Qarma();
            MyDamco();
            WarehouseWrapper();
            Apport();
          END;

  }
  CODE
  {
    VAR
      Company@100000000 : Record 2000000006;

    LOCAL PROCEDURE DocumentCapture@100000000();
    VAR
      DocumentCaptureSetup@100000000 : Record 6085573;
      DCDocumentCategory@100000001 : Record 6085575;
    BEGIN
      IF Company.FINDSET THEN REPEAT
        DocumentCaptureSetup.CHANGECOMPANY(Company.Name);
        DCDocumentCategory.CHANGECOMPANY(Company.Name);

        IF DocumentCaptureSetup.GET THEN BEGIN
          DocumentCaptureSetup."Scanned File Path for OCR" := '';
          DocumentCaptureSetup."File Path for OCR-proc. files" := '';
          DocumentCaptureSetup."PDF File Path for OCR" := '';
          DocumentCaptureSetup."PDF File Path" := '';
          DocumentCaptureSetup."TIFF File Path" := '';
          DocumentCaptureSetup."Miscellaneous File Path" := '';
          DocumentCaptureSetup."E-mail File Path" := '';
          DocumentCaptureSetup."Dynamics NAV Server Name" := '';
          DocumentCaptureSetup."Dynamics NAV Server Instance" := '';
          DocumentCaptureSetup."Dynamics NAV Server Port" := 0;
          DocumentCaptureSetup.MODIFY;
        END;

        IF DCDocumentCategory.FINDSET THEN
          DCDocumentCategory.MODIFYALL("E-mail Connection Endpoint",'');

      UNTIL Company.NEXT = 0;
    END;

    LOCAL PROCEDURE ExpenseManagement@100000001();
    VAR
      ContiniaOnlineSetup@100000000 : Record 6086500;
      ExpenseManagementSetup@100000001 : Record 6086300;
    BEGIN
      IF Company.FINDSET THEN REPEAT
        ContiniaOnlineSetup.CHANGECOMPANY(Company.Name);
        ExpenseManagementSetup.CHANGECOMPANY(Company.Name);

        IF ContiniaOnlineSetup.GET THEN BEGIN
          ContiniaOnlineSetup."Enable Web Approval" := FALSE;
          ContiniaOnlineSetup.MODIFY;
        END;

        IF ExpenseManagementSetup.GET THEN BEGIN
          ExpenseManagementSetup."Invoice Attachments Path" := '';
          ExpenseManagementSetup.MODIFY;
        END;
      UNTIL Company.NEXT = 0;
    END;

    LOCAL PROCEDURE FTP@100000002();
    BEGIN
      //None needed
    END;

    LOCAL PROCEDURE WarehouseScanner@100000003();
    VAR
      DTVScannerSetup@100000000 : Record 66001;
    BEGIN
      //Following can be used in companies in store DB

      // IF Company.FINDSET THEN REPEAT
      //  DTVScannerSetup.CHANGECOMPANY(COMPANYNAME);
      //  IF DTVScannerSetup.FINDSET THEN BEGIN
      //    DTVScannerSetup.MODIFYALL("File Path",'');
      //    DTVScannerSetup.MODIFYALL("File Name Scannerdata",'');
      //    DTVScannerSetup.MODIFYALL("File Path Scannerdata",'');
      //    DTVScannerSetup.MODIFYALL("File Process Path",'');
      //    DTVScannerSetup.MODIFYALL("Import Path",'');
      //    DTVScannerSetup.MODIFYALL("Archive Path",'');
      //    DTVScannerSetup.MODIFYALL("NAS Archive Path",'');
      //    DTVScannerSetup.MODIFYALL("NAS Process Path",'');
      //  END;
      // UNTIL Company.NEXT = 0;
    END;

    LOCAL PROCEDURE WarehouseTablet@100000004();
    BEGIN
      //None needed, handled through service adress from client
    END;

    LOCAL PROCEDURE Lector@100000005();
    VAR
      LectorIntegrationSetup@100000000 : Record 50060;
    BEGIN
      IF Company.FINDSET THEN REPEAT
        LectorIntegrationSetup.CHANGECOMPANY(Company.Name);
        IF LectorIntegrationSetup.GET THEN BEGIN
          LectorIntegrationSetup.TestMarker := TRUE;
          LectorIntegrationSetup.FTP_Host := 'test-sg-lts.lector.dk';
          LectorIntegrationSetup.MODIFY;
        END;
      UNTIL Company.NEXT = 0;
    END;

    LOCAL PROCEDURE Qarma@100000006();
    VAR
      QarmaSetup@100000000 : Record 50090;
    BEGIN
      IF Company.FINDSET THEN REPEAT
        QarmaSetup.CHANGECOMPANY(Company.Name);
        IF QarmaSetup.GET THEN BEGIN
          QarmaSetup.QarmaImportWsAddress := 'https://eu1.babelway.net/ws/gateways/567699';
          QarmaSetup.QarmaImportWsUser := 'sg_testing';
          QarmaSetup.QarmaImportWsPassword := '1NqXQrydXupxZxUG04UI';
          QarmaSetup.TestFileLocation := '\\sgfile02.grenes.local\qarmatest$';
          QarmaSetup.TestEnvironment := TRUE;
          QarmaSetup.MODIFY;
        END;
      UNTIL Company.NEXT = 0;
    END;

    LOCAL PROCEDURE MyDamco@100000007();
    VAR
      MyDamcoIntgrSetup@100000000 : Record 50097;
    BEGIN
      IF NOT MyDamcoIntgrSetup.READPERMISSION THEN
        EXIT;

      IF Company.FINDSET THEN REPEAT
        MyDamcoIntgrSetup.CHANGECOMPANY(Company.Name);
        IF MyDamcoIntgrSetup.GET THEN BEGIN
          MyDamcoIntgrSetup.TestmMarker := TRUE;
          MyDamcoIntgrSetup.RestEndpoint := 'https://core-api-emea-uat.damco.com/b2b/';
          MyDamcoIntgrSetup.RestUser := 'testuserSostrene';
          MyDamcoIntgrSetup.RestPassword := 'Sostrene@Damco123';
          MyDamcoIntgrSetup.MODIFY;
        END;
      UNTIL Company.NEXT = 0;
    END;

    LOCAL PROCEDURE JobQueue@100000008();
    VAR
      JobQueueEntry@100000000 : Record 472;
    BEGIN
      IF Company.FINDSET THEN REPEAT
        JobQueueEntry.CHANGECOMPANY(Company.Name);
        IF JobQueueEntry.FINDSET THEN
          JobQueueEntry.MODIFYALL(Status,JobQueueEntry.Status::"On Hold");
      UNTIL Company.NEXT = 0;
    END;

    LOCAL PROCEDURE LSTaskQueue@100000009();
    VAR
      SchedulerSetup@100000000 : Record 99001486;
      SchedulerJobHeader@100000001 : Record 99001586;
      SchedulerSubjob@100000002 : Record 99001588;
    BEGIN
      IF Company.FINDSET THEN REPEAT
        SchedulerSetup.CHANGECOMPANY(Company.Name);
        SchedulerJobHeader.CHANGECOMPANY(Company.Name);
        SchedulerSubjob.CHANGECOMPANY(Company.Name);

        IF SchedulerSetup.GET THEN BEGIN
          SchedulerSetup."Enable NAS Scheduler" := FALSE;
          SchedulerSetup.MODIFY;
        END;

        IF SchedulerJobHeader.FINDSET THEN BEGIN
          SchedulerJobHeader.MODIFYALL("Next Check Date",0D);
          SchedulerJobHeader.MODIFYALL("Next Check Time",0T);
        END;

        IF SchedulerSubjob.FINDSET THEN
          SchedulerSubjob.MODIFYALL(Enabled,FALSE);

      UNTIL Company.NEXT = 0;
    END;

    LOCAL PROCEDURE WarehouseWrapper@100000010();
    VAR
      OPCServer@100000000 : Record 68100;
    BEGIN
      IF Company.FINDSET THEN REPEAT
        OPCServer.CHANGECOMPANY(Company.Name);
        IF OPCServer.FINDSET THEN BEGIN
          OPCServer.MODIFYALL("Test Mode",TRUE,FALSE);
          OPCServer.MODIFYALL(URL,'',FALSE);
        END;
      UNTIL Company.NEXT = 0;
    END;

    LOCAL PROCEDURE Apport@100000011();
    VAR
      SGSetup@100000000 : Record 50050;
    BEGIN
      IF Company.FINDSET THEN REPEAT
        SGSetup.CHANGECOMPANY(Company.Name);
        IF SGSetup.GET THEN BEGIN
          SGSetup."Apport Url" := 'TEST';
          SGSetup.MODIFY;
        END;
      UNTIL Company.NEXT = 0;
    END;

    BEGIN
    {
      RIT.001 29-10-2018  BDP [2018082110000322]
                              - Created
    }
    END.
  }
}

OBJECT Page 66001 AutoEnvironmentSetup
{
  OBJECT-PROPERTIES
  {
    Date=29-08-18;
    Time=11:06:57;
    Modified=Yes;
    Version List=RIT.001,AutoEnv;
  }
  PROPERTIES
  {
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table66122;
    PageType=Card;
    OnOpenPage=BEGIN
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;
               END;

  }
  CONTROLS
  {
    { 1000000000;0;Container;
                ContainerType=ContentArea }

    { 1000000001;1;Group  ;
                Name=General;
                GroupType=Group }

    { 1000000006;2;Field  ;
                SourceExpr=SenderEmail }

    { 1000000007;2;Field  ;
                SourceExpr=SenderEmailName }

    { 1000000008;2;Field  ;
                SourceExpr=EmailSubject }

    { 1000000004;1;Group  ;
                Name=MessageType;
                CaptionML=[DAN=Beskedtyper;
                           ENU=Message Types];
                GroupType=Group }

    { 1000000005;2;Field  ;
                SourceExpr=NewTestEnvironmentMsgType }

    { 1000000002;1;Group  ;
                Name=Texts;
                CaptionML=[DAN=Tekster;
                           ENU=Texts];
                GroupType=Group }

    { 1000000003;2;Field  ;
                SourceExpr=NewTestEnvironmentText }

  }
  CODE
  {

    BEGIN
    {
      RIT.001 29-08-2018  BDP [2018082110000233]
                              - Created
    }
    END.
  }
}

